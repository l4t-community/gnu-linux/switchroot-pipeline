monitor.alsa.rules = [
    {
        matches = [
        {
            node.name = "alsa_output.platform-sound.HiFi__Speaker__sink"
        }
        ]
        actions = {
            update-props = {
                audio.format = "S16LE",
                audio.rate = 48000,
                audio.allowed-rates = 48000
            }
        }
    }
]
