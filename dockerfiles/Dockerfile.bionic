# syntax = edrevo/dockerfile-plus
ARG CODENAME
ARG BUILDVARIANT

FROM ${TARGETARCH}${BUILDVARIANT}/ubuntu:${CODENAME} as base

ENV DEBIAN_FRONTEND=noninteractive
# Prepare apt for source fetching
RUN sed -i "s/# deb-src/deb-src/g" /etc/apt/sources.list
# Prevent TZDATA interactive
RUN TZ=Europe/Paris ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt update -y && apt upgrade -y
RUN apt install -y \
    p7zip-full \
    libgnutls28-dev \
    libgmp-dev \
    libgmpxx4ldbl \
    libgnutls-dane0 \
    libgnutls-openssl27 \
    libgnutls28-dev \
    libgnutlsxx28 \
    libidn2-0-dev \
    libidn2-dev \
    libp11-kit-dev \
    libtasn1-6-dev \
    libtasn1-doc \
    nettle-dev \
    libunistring-dev \
    libv4l-dev \
    libfreetype6-dev \
    libfribidi-dev \
    libfontconfig1-dev \
    libgsm1-dev \
    libmp3lame-dev \
    libopenjp2-7-dev \
    libopus-dev \
    libcairo-script-interpreter2 \
    libcairo2-dev \
    libgdk-pixbuf2.0-dev \
    libpixman-1-dev \
    libxcb-shm0-dev \
    libxrender-dev \
    libselinux1-dev \
    libblkid-dev \
    libmount-dev \
    libffi-dev \
    libshine-dev \
    libsnappy-dev \
    libspeex-dev \
    libogg-dev \
    libtheora-dev \
    libtwolame-dev \
    libjpeg-dev \
    libjpeg-turbo8-dev \
    libjpeg8-dev \
    libvorbis-dev \
    libass-dev \
    libvpx-dev \
    libwebp-dev \
    libx264-dev \
    libx265-dev \
    libnuma-dev \
    libxvidcore-dev \
    libzvbi-dev \
    libcaca-dev \
    libpulse-dev \
    libsdl1.2-dev \
    libsdl2-dev \
    libslang2-dev \
    libfdk-aac-dev \
    autoconf \
    libgtk-3-dev \
    autopoint \
    appstream-util \
    libglib2.0-dev \
    autoconf-archive \
    automake \
    build-essential \
    cmake \
    git-core \
    librsvg2-dev \
    libfreetype6-dev \
    libgnutls28-dev \
    libsdl2-dev \
    libtool \
    libva-dev \
    libvdpau-dev \
    libvorbis-dev \
    libxcb1-dev \
    libxcb-shm0-dev \
    libxcb-xfixes0-dev \
    meson \
    ninja-build \
    pkg-config \
    texinfo \
    wget \
    yasm \
    zlib1g-dev \
    git \
    make \
    gcc \
    wget \
    tar \
    patch \
    xz-utils \
    bc \
    xxd \
    build-essential \
    bison \
    flex \
    python3 \
    python3-distutils \
    python3-dev \
    swig \
    kmod \
    curl \
    u-boot-tools \
    device-tree-compiler \
    ccache \
    unzip \
    xz-utils \
    advancecomp \
    apt \
    base-files \
    binutils \
    binutils-common \
    bzip2 \
    ca-certificates \
    cpp \
    cpp-7 \
    e2fsprogs \
    g++ \
    g++-7 \
    gcc \
    gcc-7 \
    gcc-7-base \
    gcc-8-base \
    gpg \
    gpg-agent \
    gpgconf \
    gpgv \
    libapparmor1 \
    libasan4 \
    libatomic1 \
    libbinutils \
    libbz2-1.0 \
    libc-bin \
    libc-dev-bin \
    libc6 \
    libc6-dev \
    libcc1-0 \
    libcom-err2 \
    libdb5.3 \
    libext2fs2 \
    libgcc-7-dev \
    libgcc1 \
    libgcrypt20 \
    libgnutls30 \
    libgomp1 \
    libidn2-0 \
    libitm1 \
    liblsan0 \
    libopus-dev \
    libpng16-16 \
    libseccomp2 \
    libsqlite3-0 \
    libss2 \
    libssl1.1 \
    libstdc++-7-dev \
    libstdc++6 \
    libsystemd0 \
    libtsan0 \
    libubsan0 \
    libudev1 \
    libzstd1 \
    linux-libc-dev \
    openssl \
    patch \
    perl \
    perl-base \
    pkgbinarymangler \
    procps \
    systemd \
    systemd-sysv \
    tzdata \
    libllvm9 \
    dpkg-dev \
    apt-utils \
    zstd \
    alien \
    devscripts \
    equivs \
    software-properties-common

RUN apt-get -y build-dep chromium-browser
RUN apt-get -y build-dep ffmpeg

# Setup qckdeb
RUN wget -O /usr/local/bin/qckdeb 'https://gitlab.com/unity-x/qckdeb/-/raw/master/qckdeb?inline=false'
RUN chmod 755 /usr/local/bin/qckdeb
RUN sed -i 's/\[ ! -z "$pkgdeps" \]/\[ ! -z "$conflicts" \] \&\& echo "Conflicts: ${conflicts}" >> "${DEBIAN}\/control" \|\| true\n\[ ! -z "$provides" \] \&\& echo "Provides: ${provides}" >> "${DEBIAN}\/control" \|\| true\n\[ ! -z "$suggests" \] \&\& echo "Suggests: ${suggests}" >> "${DEBIAN}\/control" \|\| true\n\[ ! -z "$pkgdeps" \]/g; s/build .* true/build/g; s/package .* true//g' /usr/local/bin/qckdeb

# Extract BSP
RUN wget https://developer.nvidia.com/embedded/dlc/r32-3-1_Release_v1.0/t210ref_release_aarch64/Tegra210_Linux_R32.3.1_aarch64.tbz2 \
	&& wget https://repo.download.nvidia.com/jetson/t210/pool/main/n/nvidia-l4t-jetson-multimedia-api/nvidia-l4t-jetson-multimedia-api_32.3.1-20191209225816_arm64.deb \
	&& tar xf Tegra210_Linux_R32.3.1_aarch64.tbz2 \
	&& mv Linux_for_Tegra/bootloader/*.deb Linux_for_Tegra/kernel/*.deb Linux_for_Tegra/nv_tegra/l4t_deb_packages/*.deb . \
	&& for deb in $(ls *.deb); do 7z x $deb -y; tar -h -xf data.tar.* -C /; rm $deb; done \
	&& cp etc/apt/trusted.gpg.d/jetson-ota-public.asc /etc/apt/trusted.gpg.d/jetson-ota-public.asc \
	|| cp Linux_for_Tegra/nv_tegra/jetson-ota-public.key /etc/apt/trusted.gpg.d/jetson-ota-public.asc \
	&& chmod 644 /etc/apt/trusted.gpg.d/jetson-ota-public.asc \
	&& rm -rf Linux_for_Tegra/ Tegra210_Linux_R32.3.1_aarch64.tbz2 nvidia-l4t-jetson-multimedia-api_32.3.1-20191209225816_arm64.deb /control.tar.* /data.tar.* debian-binary /boot \
	&& sed -i "s/<SOC>/t210/g" /etc/apt/sources.list.d/*.list

# Make ld.so.conf
RUN echo "/usr/lib/aarch64-linux-gnu/tegra" > /etc/ld.so.conf.d/nvidia-tegra.conf && \
	echo "/usr/lib/aarch64-linux-gnu/tegra-egl" >> /etc/ld.so.conf.d/nvidia-tegra.conf && \
	ldconfig

FROM base as build_arm64_bionic
ONBUILD RUN apt install -y libunbound2 libsepol1-dev gcc-arm-linux-gnueabi zstd binutils-aarch64-linux-gnu

FROM build_${TARGETARCH}_${CODENAME}
INCLUDE+ dockerfiles/Dockerfile.base
