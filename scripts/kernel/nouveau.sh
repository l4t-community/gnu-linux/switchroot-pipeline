#!/bin/bash
set -e

# Build variables
export KERNEL_DIR="/out/kernel"
export BRANCH="${BRANCH:-"icosa-v6.6"}"
export VERSION="${VERSION:-"6.6"}"

prepare() {
	mkdir -p "${KERNEL_DIR}" "${OUT}/nouveau"

	echo -e "Cloning Mainline Nouveau kernel from l4t-community if missing\n"
	if [[ -z $(ls -A ${KERNEL_DIR}/kernel-nouveau) ]]; then
		git clone -b ${BRANCH} https://gitlab.com/l4t-community/kernel/mainline/linux "${KERNEL_DIR}/kernel-nouveau/"
		cd "${KERNEL_DIR}/kernel-nouveau/"
	else
		cd "${KERNEL_DIR}/kernel-nouveau/"
		git reset --hard
		git fetch
		git pull
		git checkout ${BRANCH}
	fi
}

createUpdateModules() {
	echo -e "Create compressed modules and update archive with correct permissions and ownership\n"
	find "$1" -type d -exec chmod 755 {} \;
	find "$1" -type f -exec chmod 644 {} \;
	find "$1" -name "*.sh" -type f -exec chmod 755 {} \;
	fakeroot chown -R root:root "$1"
	tar -C "$1" -czvpf "$2" .
}

build() {
	echo -e "Preparing Source and Creating Defconfig\n"
	make nintendo_switch_defconfig 

	make -j"${CPUS}" prepare
	make -j"${CPUS}" Image
	make -j"${CPUS}" dtbs
	make -j"${CPUS}" modules

	mkimage -A arm64 -O linux -T kernel -C none -a 0x80200000 -e 0x80200000 -n "AZKRN-${VERSION}" -d ${KERNEL_DIR}/kernel-nouveau/arch/arm64/boot/Image "${OUT}/nouveau/uImage"

	mkdtimg create "${OUT}/nouveau/nx-plat.dtimg" --page_size=1000 \
		${KERNEL_DIR}/kernel-nouveau/arch/arm64/boot/dts/nvidia/tegra210-odin.dtb --id=0x4F44494E

	make modules_install INSTALL_MOD_PATH="${KERNEL_DIR}/modules/"
	make headers_install INSTALL_HDR_PATH="${KERNEL_DIR}/update/usr/"
	make -j"${CPUS}" clean
}

postConfig() {
	echo -e "Refresh permissions for kernel headers and Create compressed modules and headers\n"
	find "${KERNEL_DIR}/update/usr/include" -exec chmod 777 {} \;
    find "${KERNEL_DIR}/update/usr/include" \
    \( -name .install -o -name .check -o \
        -name ..install.cmd -o -name ..check.cmd \) | xargs rm -f

	createUpdateModules "${KERNEL_DIR}/modules/lib/" "${OUT}/nouveau/modules.tar.gz"
	createUpdateModules "${KERNEL_DIR}/update/" "${OUT}/nouveau/update.tar.gz"

	rm -rf "${KERNEL_DIR}/modules" "${KERNEL_DIR}/update/usr/include"
	echo "Done"
}

createKernelSources() {
    cd "${KERNEL_DIR}"
    7z a '-xr!.git' '-xr!.*' '-xr!*.o' '-xr!*.dtb' '-xr!vmlinux' '-xr!Image' "${OUT}/kernel-${VERSION}-src.7z" kernel-nouveau
}

#
# PREPARE
#
prepare

#
# BUILD
#
build

#
# PACKAGE
#
postConfig

#
# KERNEL SRC
#
createKernelSources
