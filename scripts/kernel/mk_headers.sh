#!/bin/bash
version=$(echo ${KERNEL_BRANCH#"linux-"} | cut -d '_' -f1)

cd "/build/packages/debian/linux-headers"
sed -i 's/pkgver=.*/pkgver='${version}'/g' DEBBUILD
sed -i 's/Version:.*/Version: '${version}'/g' debian/control

dch --create -v ${version} --package linux-headers -c debian/changelog ""
qckdeb
cp *.deb ${OUT}
