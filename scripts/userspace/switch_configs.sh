#!/bin/bash
mkdir -p ${OUT} 
cd ${OUT}

# Switch specific configs
for dir in /build/switch-l4t-configs/*; do
	dir=${dir##*/}
	if [[ ${dir} != "nvidia-l4t-bsp" ]] ;then
		mkdir -p ${dir}
		cp -r /build/switch-l4t-configs/${dir}/* ${dir}/
	fi
done

mkdir -p nvidia-l4t-bsp configs configs_mainline
# Remove non override bits from nvidia-l4t-bsp
rm -rf /build/switch-l4t-configs/nvidia-l4t-bsp/{init,firmware,nvpmodel}
cp -r /build/switch-l4t-configs/nvidia-l4t-bsp/*/* nvidia-l4t-bsp/
tar czf "${OUT}/nvidia_l4s_bsp.tar.gz" -C nvidia-l4t-bsp .

# Cleanup
rm -rf switch-l4t-configs.tar switchroot-pipeline-master-switch-l4t-configs switch-l4t-configs nvidia-l4t-bsp

echo -e "Creating l4s_configs disk image\n"
cp switch-dock-handler/etc/skel/.config/monitors_l4t.xml switch-dock-handler/etc/skel/.config/monitors.xml
cp switch-dock-handler/root/.config/monitors_l4t.xml switch-dock-handler/root/.config/monitors.xml

rm switch-dock-handler/etc/skel/.config/monitors_l4t.xml switch-dock-handler/root/.config/monitors_l4t.xml

cp -r switch-*/* configs

tar czf "${OUT}/l4s_configs.tar.gz" -C configs .

echo -e "Creating l4s_configs_mainline disk image\n"
rm -rf switch-xorg-conf/etc/X11/xorg.conf.d/11-nvidia.conf

cp switch-dock-handler/etc/skel/.config/monitors_mainline.xml switch-dock-handler/etc/skel/.config/monitors.xml
cp switch-dock-handler/root/.config/monitors_mainline.xml switch-dock-handler/root/.config/monitors.xml

rm switch-dock-handler/etc/skel/.config/monitors_mainline.xml switch-dock-handler/root/.config/monitors_mainline.xml
rm -rf switch-r2c-utils switch-initial-setup-fedora switch-chromium-nvdec-assets

cp -r switch-*/* configs_mainline

tar czf "${OUT}/l4s_configs_mainline.tar.gz" -C configs_mainline .
