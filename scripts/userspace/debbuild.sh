#!/bin/bash
export NAME=$(grep -oP '(?<=UBUNTU_CODENAME=).*' /etc/os-release)
if [ "${NAME}" = "bionic" ]; then
		add-apt-repository ppa:eh5/pulseaudio-a2dp
		wget -qO - https://jetson.repo.azka.li/ubuntu/pubkey | apt-key add -
		add-apt-repository -y 'deb https://jetson.repo.azka.li/ubuntu bionic main'
		apt update -y && apt upgrade -y
fi

cd "/build/packages/debian/${2}"

if [[ $1 == "qckdeb" ]]; then
	export version=$(grep "^Version: .*$" debian/control | sed 's/Version: //g')
	dch --create -v ${version} --package ${2} -c debian/changelog ""
	qckdeb
	cp *.deb /out
elif [[ $1 == "debuild" ]]; then
	mk-build-deps
	apt install ./*.deb
	rm *.deb
	debuild --no-lintian -b -us -uc -d
else
	echo -e "Not a valid build method\n"
	exit 1
fi

cp *.deb /out
