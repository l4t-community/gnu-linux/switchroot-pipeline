#!/bin/bash
# Dependencies
# libsdl2-2.0-0, libqt5core5a, libqt5dbus5, libqt5gui5, libqt5network5, libqt5widgets5, libqt5xml5

git clone libass 
git clone ffmpeg
git clone mpv
git clone smplayer

# libass
cd libass
./autogen.sh --prefix="${OUT}/ffmpeg_l4t" --libdir="${OUT}/ffmpeg_l4t/usr/lib" --enable-static --disable-shared
./configure  --prefix="${OUT}/ffmpeg_l4t" --libdir="${OUT}/ffmpeg_l4t/usr/lib" --enable-static --disable-shared
make install -j${CPUS}
cd ..

# ffmpeg-l4t
cd ffmpeg
PKG_CONFIG_PATH="${OUT}/ffmpeg_l4t/usr/lib/pkgconfig" ./configure \
 --prefix="${OUT}/ffmpeg_l4t" \
 --pkg-config-flags="--static" \
 --extra-cflags="-march=armv8-a+simd+crypto+crc -mtune=cortex-a57 -I/usr/src/jetson_multimedia_api/include" \
 --extra-ldflags="-L$OUT/ffmpeg_l4t/lib -L/usr/lib/aarch64-linux-gnu/tegra" \
 --extra-libs="-lpthread -lm -lnvbuf_utils -lv4l2" \
 --ld="g++" \
 --bindir="$OUT/ffmpeg_l4t/bin" \
 --disable-logging \
 --enable-gpl \
 --enable-gnutls \
 --enable-static \
 --enable-librsvg \
 --enable-libv4l2 \
 --enable-libgsm \
 --enable-libopenjpeg\
 --enable-libshine \
 --enable-libsnappy \
 --enable-libspeex \
 --enable-libtheora \
 --enable-libtwolame \
 --enable-libwebp \
 --enable-libxvid \
 --enable-libzvbi \
 --enable-libass \
 --enable-libfreetype \
 --enable-libmp3lame \
 --enable-libopus \
 --enable-libvorbis \
 --enable-libvpx \
 --enable-libx264 \
 --enable-libx265 \
 --enable-ladspa \
 --enable-libbluray \
 --enable-libbs2b \
 --enable-libcaca \
 --enable-libcdio \
 --enable-libcodec2 \
 --enable-libflite \
 --enable-libfontconfig \
 --enable-libfreetype \
 --enable-libfribidi \
 --enable-libgme \
 --enable-libgsm \
 --enable-libjack \
 --enable-libmp3lame \
 --enable-libopenjpeg \
 --enable-libopenmpt \
 --enable-libopus \
 --enable-libpulse \
 --enable-librubberband \
 --enable-libshine \
 --enable-libsnappy \
 --enable-libsoxr \
 --enable-libspeex \
 --enable-libssh \
 --enable-libtheora \
 --enable-libtwolame \
 --enable-libvorbis \
 --enable-libvpx \
 --enable-libwebp \
 --enable-libx265 \
 --enable-libxml2 \
 --enable-libxvid \
 --enable-libzvbi \
 --enable-lv2 \
 --enable-openal \
 --enable-opengl \
 --enable-sdl2 \
 --enable-nvv4l2

make -j${CPUS}
make install
cd ..

-----
//ffmpeg other options
 # libs .so
 --enable-pic \
 --enable-shared \

  # both libs .so and .a
 --enable-pic \
 --enable-static \
 --enable-shared \

# static binaries
 --pkg-config-flags="--static" \

 --enable-libwavpack \ //for 3.4.8
-----

# MPV
cd mpv
export MPV_LDLIBS="-lpthread -lm -lnvbuf_utils -lv4l2 -lxml2 -llzma -lopenal -lsoxr -lbz2 -lopenmpt -lgme -lGL -lcdio_paranoia -lcdio_cdda -lcdio -lvpx -lpthread -lwebpmux -lwebp -pthread -lrsvg-2 -lgio-2.0 -lz -lresolv -lselinux -lmount -lgdk_pixbuf-2.0 -lgmodule-2.0 -ldl -lpng16 -lcairo -lgobject-2.0 -lffi -lglib-2.0 -lpcre -lpixman-1 -lfontconfig -lexpat -lfreetype -lxcb-shm -lxcb-render -lXrender -lXext -lX11 -lxcb -lXau -lXdmcp -lzvbi -lpng -lsnappy -lstdc++ -lcodec2 -lgsm -lmp3lame -lopenjp2 -lopus -lshine -lspeex -ltheoraenc -ltheoradec -logg -ltwolame -lvorbis -lvorbisenc -lx264 -lx265 -lrt -lnuma -lxvidcore -lv4lconvert -ljpeg -lva -licui18n -licuuc -licudata -lmpg123 -lvorbisfile -lbluray -lgnutls -lgmp -lidn2 -lhogweed -lnettle -ltasn1 -lp11-kit -lssh -lbs2b -llilv-0 -lsratom-0 -lsord-0 -lserd-0 -lrubberband -lflite_cmu_time_awb -lflite_cmu_us_awb -lflite_cmu_us_kal -lflite_cmu_us_kal16 -lflite_cmu_us_rms -lflite_cmu_us_slt -lflite_usenglish -lflite_cmulex -lflite -lfribidi -lass -lharfbuzz -lgraphite2 -lz"

PKG_CONFIG_PATH="${OUT}/ffmpeg_l4t/usr/lib/pkgconfig" LDFLAGS="${MPV_LDLIBS} $(pkg-config --libs fontconfig harfbuzz fribidi) -L${OUT}/ffmpeg_l4t/usr/lib -L/usr/lib/aarch64-linux-gnu/tegra" ./bootstrap.py

PKG_CONFIG_PATH="${OUT}/ffmpeg_l4t/usr/lib/pkgconfig" LDFLAGS="${MPV_LDLIBS} $(pkg-config --libs fontconfig harfbuzz fribidi) -L${OUT}/ffmpeg_l4t/usr/lib -L/usr/lib/aarch64-linux-gnu/tegra" python3 ./waf configure --enable-libmpv-static --disable-debug-build

PKG_CONFIG_PATH="${OUT}/ffmpeg_l4t/usr/lib/pkgconfig" LDFLAGS="${MPV_LDLIBS} $(pkg-config --libs fontconfig harfbuzz fribidi) -L${OUT}/ffmpeg_l4t/usr/lib -L/usr/lib/aarch64-linux-gnu/tegra" python3 ./waf build

INSTALL="${OUT}/ffmpeg_l4t/mpv" ./waf install

strip -o mpv-l4t mpv
cd ..

# smplayer
cd smplayer
export QT_SELECT=qt5
make src/smplayer -j${CPUS}
DESTDIR="${OUT}/ffmpeg_l4t/smplayer" make install
cd ../
