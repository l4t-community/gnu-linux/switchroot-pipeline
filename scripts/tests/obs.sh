#!/bin/bash
set -xe
export VER=${VER:-"39"}
export OUT=$(realpath ${OUT})

build_obs() {
pushd $1
osc build \
        -j $(nproc) \
        -t $(nproc) \
        --vm-type=chroot \
        --noverify \
        --noservice \
        --no-changelog \
        --nodebugpackages \
        --disable-debuginfo \
        --download-api-only \
        --trust-all-projects \
        aarch64 Fedora_${VER}
popd
}

osc co home:Azkali:Tegra:Icosa
build_obs home\:Azkali\:Tegra\:Icosa/l4s-bsp/
build_obs home\:Azkali\:Tegra\:Icosa/l4s-bsp-kernel/
7z a "${OUT}/bsp.7z" /var/tmp/build-root/Fedora_${VER}-aarch64/.mount/home/abuild/rpmbuild/RPMS/aarch64/*.rpm
