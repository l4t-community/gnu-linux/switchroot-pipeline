#!/bin/bash

# Output dir
mkdir -p "${OUT}/vlc_bionic/"

# Setup ccache
/usr/sbin/update-ccache-symlinks
export PATH="/usr/lib/ccache:$PATH"

# Get sources
apt update
apt-get source vlc

cd vlc-3.0.8

# Apply patches
sed -i 's/--enable-omxil/--disable-omixl/g' debian/rules
patch -p1 -i /build/patches/userspace/vlc/0001-codec-avcodec-use-filtered-best-effort-pts.patch

# Ignore querrying package info
echo -e "\noverride_dh_shlibdeps:\n\tdh_shlibdeps --dpkg-shlibdeps-params=--ignore-missing-info" >> debian/rules

# Commit dpkg changes
EDITOR=/bin/true dpkg-source -q --commit . 0001-codec-avcodec-use-filtered-best-effort-pts.patch

# Bump version
sed -i "s/3.0.8-0ubuntu18.04.1/3.0.8-1ubuntu18.04.1l4t/g" debian/changelog
sed -i '/libomxil_plugin.so/d' debian/vlc-plugin-base.install

export CFLAGS+="-march=armv8-a+simd+crypto -mtune=cortex-a57"

# Build debs
DEB_BUILD_OPTIONS=nocheck dpkg-buildpackage -j"${CPUS}"
cd ..

# Copy build to output
cp -r *.deb "${OUT}/vlc_bionic/"
