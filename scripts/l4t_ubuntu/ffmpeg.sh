#!/bin/bash
set -e

# Global environment
export PREFIX="${OUT}/prefix/"
export BUILD="/opt/build"
export DESTDIR="${PREFIX}"
export PKG_CONFIG_PATH="${PKG_CONFIG_PATH}:${PREFIX}/usr/lib/aarch64-linux-gnu/pkgconfig/:${PREFIX}/usr/share/pkgconfig/"

# Create build, libs and install paths
mkdir -p "${PREFIX}" "${BUILD}"

#
# jetson-ffmpeg
#
git clone https://gitlab.com/switchroot/switch-l4t-multimedia/jetson-ffmpeg "${BUILD}/jetson-ffmpeg"
cd "${BUILD}/jetson-ffmpeg"

# Remove after merged
git apply /build/patches/userspace/jetson-ffmpeg.patch

mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX="/usr" ..
make -j"${CPUS}"
make install
ldconfig

#
# FFmpeg
#
git clone https://gitlab.com/switchroot/switch-l4t-multimedia/FFmpeg "${BUILD}/ffmpeg"
cd "${BUILD}/ffmpeg"
./configure \
	--prefix="${PREFIX}" \
	--pkg-config-flags="--static" \
	--extra-cflags="-march=armv8-a+simd+crypto -mtune=cortex-a57 -I/usr/src/jetson_multimedia_api/include" \
	--extra-ldflags="-L${PREFIX}/usr/lib/ -L/usr/lib/aarch64-linux-gnu/tegra" \
	--extra-libs="-lpthread -lm -lnvbuf_utils -lv4l2" \
	--ld="g++" \
	--bindir="${PREFIX}/usr/bin" \
	--disable-logging \
	--enable-gpl \
	--enable-gnutls \
	--enable-static \
	--enable-librsvg \
	--enable-libv4l2 \
	--enable-libgsm \
	--enable-libopenjpeg\
	--enable-libshine \
	--enable-libsnappy \
	--enable-libspeex \
	--enable-libtheora \
	--enable-libtwolame \
	--enable-libwebp \
	--enable-libxvid \
	--enable-libzvbi \
	--enable-libass \
	--enable-libfreetype \
	--enable-libmp3lame \
	--enable-libopus \
	--enable-libvorbis \
	--enable-libvpx \
	--enable-libx264 \
	--enable-libx265 \
	--enable-indev=v4l2 \
	--enable-nvmpi \
	--enable-nvv4l2dec

make -j"${CPUS}"
make install
ldconfig

# MPV and SMPV-Player LDFLAGS
export LDFLAGS="-lpthread -lm -lnvbuf_utils -lv4l2 $(pkg-config --libs fontconfig harfbuzz fribidi) -L${PREFIX}/usr/lib/ -L/usr/lib/aarch64-linux-gnu/tegra"

#
# MPV
#
git clone https://gitlab.com/switchroot/switch-l4t-multimedia/mpv "${BUILD}/mpv"
cd "${BUILD}/mpv"
./bootstrap.py
python3 ./waf configure --enable-libmpv-static --disable-debug-build
python3 ./waf build
INSTALL="${PREFIX}/bin/mpv" ./waf install

#
# smpv-player
#
git clone https://gitlab.com/switchroot/switch-l4t-multimedia/smpv-player "${BUILD}/smpv-player"
export QT_SELECT=qt5
cd "${BUILD}/smpv-player"
make src/smplayer -j"${CPUS}"
make install
