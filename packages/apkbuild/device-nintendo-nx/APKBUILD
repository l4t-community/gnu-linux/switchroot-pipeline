# Maintainer: Ultracoolguy <ultracoolguy@disroot.org>
# Co-Maintainer: Azkali Manad <a_random_mailer@protonmail.com>
# Reference: <https://postmarketos.org/devicepkg>
pkgname=device-nintendo-nx
pkgdesc="Nintendo Switch"
pkgver=0.2
pkgrel=0
url="https://postmarketos.org"
license="MIT"
arch="aarch64"
options="!check !archcheck"
depends="postmarketos-base linux-nintendo-nx linux-nintendo-nx-headers joycond"
makedepends="devicepkg-dev"
hekate_version=5.5.6
nyx_version=1.0.3
hekate_bin="$srcdir/hekate_ctcaer_${hekate_version}.bin"
install="$pkgname.post-install"
source="deviceinfo
	switch-l4t-configs.tar::http://gitlab.com/l4t-community/gnu-linux/switch-l4t-configs/-/archive/master/switch-l4t-configs-master.tar.gz
	https://github.com/CTCaer/hekate/releases/download/v${hekate_version}/hekate_ctcaer_${hekate_version}_Nyx_${nyx_version}.zip"
subpackages="$pkgname-nonfree-firmware:nonfree_firmware nintendo-nx-configs"

build() {
	devicepkg_build $startdir $pkgname
}

package() {
	devicepkg_package $startdir $pkgname
}

nonfree_firmware() {
	pkgdesc="Nvidia utilities + firmware. Probably won't work without it"
	depends="firmware-nintendo-nx
		firmware-nintendo-nx-camera
		firmware-nintendo-nx-ccp_t210ref
		firmware-nintendo-nx-configs
		firmware-nintendo-nx-core
		firmware-nintendo-nx-firmware
		firmware-nintendo-nx-graphics_demos
		firmware-nintendo-nx-gstreamer
		firmware-nintendo-nx-jetson_multimedia_api
		firmware-nintendo-nx-kernel_dtbs
		firmware-nintendo-nx-multimedia
		firmware-nintendo-nx-multimedia_utils
		firmware-nintendo-nx-oem_config
		firmware-nintendo-nx-tools
		firmware-nintendo-nx-wayland
		firmware-nintendo-nx-weston
		firmware-nintendo-nx-x11
		firmware-nintendo-nx-xusb_firmware"
	mkdir "$subpkgdir"
}

configs() {
	pkgdesc="Nintendo Switch configs"
	mkdir -p $subpkgdir/etc/X11/xorg.conf.d/ \
	$subpkgdir/etc/dconf/db/local.d/ \
	$subpkgdir/etc/dconf/profile/ \
	$subpkgdir/etc/sddm.conf.d/ \
	$subpkgdir/etc/lightdm/lightdm.conf.d/ \
	$subpkgdir/etc/xdg/autostart/ \
	$subpkgdir/usr/bin/ \
	$subpkgdir/usr/lib/firmware \
	$subpkgdir/usr/lib/udev/rules.d/ \
	$subpkgdir/usr/share/onboard/ \
	$subpkgdir/usr/share/alsa/ucm2/tegra-snd-t210ref-mobile-rt565x/ \
	$subpkgdir/var/lib/alsa/

	# Hekate bin
	install ${hekate_bin} $subpkgdir/usr/lib/firmware/reboot_payload.bi

	mv $srcdir/switch-l4t-configs-* $srcdir/switch-l4t-configs
	cd $srcdir

	# Dock-hotplug
	install switch-l4t-configs/switch-dock-handler/92-dp-switch.rules $subpkgdir/usr/lib/udev/rules.d/
	install switch-l4t-configs/switch-dock-handler/dock-hotplug $subpkgdir/usr/bin/
	ls $srcdir/switch-l4t-configs/switch-dock-handler
	install switch-l4t-configs/switch-dock-handler/nintendo-switch-display.desktop $subpkgdir/etc/xdg/autostart/
	sed 's/sudo -u/sudo -s -u/g' -i $subpkgdir/usr/bin/dock-hotplug

	# Dconf customizations
	install switch-l4t-configs/switch-dconf-customizations/99-switch $subpkgdir/etc/dconf/db/local.d/
	install switch-l4t-configs/switch-dconf-customizations/user $subpkgdir/etc/dconf/profile/

	# Alsa UCM2 profile
	install switch-l4t-configs/switch-alsa-ucm2/tegra-snd-t210ref-mobile-rt565x.conf $subpkgdir/usr/share/alsa/ucm2/tegra-snd-t210ref-mobile-rt565x/
	install switch-l4t-configs/switch-alsa-ucm2/HiFi.conf $subpkgdir/usr/share/alsa/ucm2/tegra-snd-t210ref-mobile-rt565x/
	install switch-l4t-configs/switch-alsa-ucm2/asound.state $subpkgdir/var/lib/alsa/

	# Lightdm conf
	install switch-l4t-configs/switch-lightdm-conf/10-dock-hotplug.conf $subpkgdir/etc/lightdm/lightdm.conf.d/
	install switch-l4t-configs/switch-lightdm-conf/20-onboard.conf $subpkgdir/etc/lightdm/lightdm.conf.d/

	# Onboard conf
	install switch-l4t-configs/switch-onboard-conf/onboard-default-settings.gschema.override.example $subpkgdir/usr/share/onboard/

	# Touchscreen rules
	install switch-l4t-configs/switch-touch-rules/99-switch-touchscreen.rules $subpkgdir/usr/lib/udev/rules.d/

	# SDDM rule
	install switch-l4t-configs/switch-sddm-rule/69-nvidia-seat.rules $subpkgdir/usr/lib/udev/rules.d/

	# Xorg conf
	install switch-l4t-configs/switch-xorg-conf/11-nvidia.conf $subpkgdir/etc/X11/xorg.conf.d/
}


sha512sums="
ca61caca7be42dab4dd83e797407ad31a5dbcfccc5d8bf04ee612a58a311899bdc937e3ac0a558f240119de261e4c5fa3085539857d3df47ede997c305b3afa6  deviceinfo
e708ddb97d921d9700e07819c76fc76971c930417b46dca20c6060d8ad2340ed115390b28b6473127a27228e3d526b066087105e6423181925d56a476526e519  switch-l4t-configs.tar
149d9404c59145abb88eb177cc52b6eb2810091d30494ada1b936539a85bd5642062ea645b3e4f65f855793d87de8afd405426b52f63cec05f8a67e902193a54  hekate_ctcaer_5.5.6_Nyx_1.0.3.zip
"
